#include <stdio.h>
#include <conio.h>

int main(){
		float p, l, t, vol, luas;
		printf("==============================\n");
		printf("Bangun Ruang Balok\n");
		printf("==============================\n");
		printf("Masukan Panjang Balok : ");scanf("%f", &p);
		printf("Masukan Lebar Balok : ");scanf("%f", &l);
		printf("Masukan Tinggi Balok : ");scanf("%f", &t);
		
		vol = p*l*t;
		luas = 2*((p*l) + (p*t) + (l*t));
		
		printf("Masukan Volume Balok : %.2f \n",vol);
		printf("Masukan Luas Permukaan Balok : %.2f \n\n\n", luas);
		//Yuana
//-------------------------------------------------------------------------
		int sisi,vol_Kubus,LP_Kubus;
		printf("==============================\n");
		printf("Bangun Ruang Kubus\n");
		printf("==============================\n");
		printf("Masukan Besar sisi : ");
		scanf("%d",&sisi);
		
		vol_Kubus = sisi * sisi * sisi;
		LP_Kubus = 6 *(sisi*sisi);
		
		printf("Volume Kubus %d\n",vol_Kubus);
		printf("Luas Permukaan Kubus %d\n\n\n",LP_Kubus);
		//Aris
//-------------------------------------------------------------------------
		int r;
		float luas1, volume;
		printf("==============================\n");
		printf("Bangun Ruang Bola\n");
		printf("==============================\n");
		printf("Masukan Nilai Radius : "); scanf("%d", &r);
			luas1 = 4*3.14*r*r;
			volume = (4*3.14*r*r*r)/3;
		printf("Luas = %f\n",luas1);
		printf("Volume = %f\n\n\n", volume);
		//Yusriel
//-------------------------------------------------------------------------
		int r2, t2, s2, luasAlas, luasSelimut;
		float luas2, volume2;
		printf("==============================\n");
		printf("Bangun Ruang Kerucut\n");
		printf("==============================\n");
		printf("Masukan Jari-Jari Alas Kerucut : "); scanf("%d", &r2);
		printf("Masukan Tinggi Kerucut: "); scanf("%d", &t2);
		printf("Masukan Kemiringan Kerucut: "); scanf("%d", &s2);
   			luasAlas = 3.14*r2*r2;
   			luasSelimut = 3.14*r2*s2;
   			luas2 = luasAlas*luasSelimut;
   			volume2 = (luasAlas*t2)/3;
		printf("Luas Alas Kerucut = %f\n",luas2);
		printf("Volume Kerucut = %f", volume2);
		//Intan
//-------------------------------------------------------------------------
        int a, b, t3;
		float luastps, vollimas, volprm;
		
		printf("==============================\n");
		printf("Bangun Ruang Limas dan Prisma Trapesium\n");
		printf("==============================\n");
		printf("Masukkan Sisi Atas Trapesium : "); scanf("%d", &a);
		printf("Masukkan Sisi Bawah Trapesium : "); scanf("%d", &b);
		printf("Masukkan Tinggi Trapesium : "); scanf("%d", &t3);
		
		luastps=((a+b)*t3)/2;
		vollimas=(luastps*t3)/3;
		volprm=(luastps*t3);
		
		printf("Volume Limas Trapesium : %.2f satuan volume\n", vollimas);
		printf("Volume Prisma Trapesium : %.2f satuan volume\n", volprm);
	//Hana
//-------------------------------------------------------------------------

		float a2, t4, luaslim, vollim; 
		printf("============================================\n"); 
		printf("Program Menghitung Luas Permukaan dan Volume\n");
		printf("\nBangun ruang Limas\n"); 
		printf("============================================\n"); 
		printf("\nMasukan Alas Limas : "); scanf("%f", &a2); 
		printf("\nMasukan Tinggi Limas : "); scanf("%f", &t4);
		
		vollim = 0.5*(a2*t4);
	
		printf("\nMasukan Volume Limas : %.2f \n", vollim);
		
	//Hasna
//-------------------------------------------------------------------------
	int d, t5;
	float phi, voltab, luastab;
	
	phi = 3.14;
	
	printf("==============================\n");
	printf("Bangun Ruang Tabung\n");
	printf("==============================\n");
	printf("Masukan Tinggi Tabung : ");scanf("%f", &t5);
	printf("Masukan Diameter Tabung : ");scanf("%f", &d);
	
	voltab = phi*d*t5;
	
	luastab = 2*phi*d*t5;
	
	printf("Volume Tabung adalah : %.2f cm^2\n", voltab);
	printf("Luas Tabung adalah : %.2f cm^2\n", luastab);
	
	getch();
	//Novia
//-------------------------------------------------------------------------
		return 0;
}
